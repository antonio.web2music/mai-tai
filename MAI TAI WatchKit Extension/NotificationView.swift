//
//  NotificationView.swift
//  MAI TAI WatchKit Extension
//
//  Created by Antonio Cangiano on 13/01/2020.
//  Copyright © 2020 Antonio Cangiano. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
#endif

//
//  HostingController.swift
//  MAI TAI WatchKit Extension
//
//  Created by Antonio Cangiano on 13/01/2020.
//  Copyright © 2020 Antonio Cangiano. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}

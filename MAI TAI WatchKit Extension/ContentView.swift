//
//  ContentView.swift
//  MAI TAI WatchKit Extension
//
//  Created by Antonio Cangiano on 13/01/2020.
//  Copyright © 2020 Antonio Cangiano. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
